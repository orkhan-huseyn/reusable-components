import Pagination from './components/Pagination';

function App() {
  function handlePageChange(page) {
    console.log(page);
  }

  return (
    <Pagination
      nextLabel="Növbəti"
      prevLabel="Əvvəlki"
      onPageChange={handlePageChange}
      total={50}
    />
  );
}

export default App;
